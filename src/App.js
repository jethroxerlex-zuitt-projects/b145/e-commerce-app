import { UserProvider } from './UserContext'
import { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router } from 'react-router-dom'
import React, { useMemo } from 'react'

import Table from './components/Table';
import { Routes, Route } from 'react-router-dom'
import AppNavbar from './components/AppNavbar'
import Register from './pages/Register'
import Error from './pages/Error'
import Home from './pages/Home'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Products from './pages/Products'
import Cart from './pages/Cart'
import SpecificProduct from './pages/SpecificProduct'
import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    let token = localStorage.getItem('token');
    fetch('https://capstone-2-app.herokuapp.com/users/details', {
      method: "POST",
      headers: {
        "Authorization": `Bearer ${token}`
      }
    })
      .then(result => result.json())
      .then(result => {
        if (typeof result._id !== "undefined") {
          setUser({
            id: result._id,
            isAdmin: result.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
      })
  }, [])

  return (

    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="/products" element={<Products />} />
            <Route exact path="/products/:productId" element={<SpecificProduct/>} />
            <Route exact path="*" element={<Error />} />
            <Route exact path="/cart" element={<Cart />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
