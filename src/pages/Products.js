// import { Fragment, useEffect, useState } from 'react'
// import ProductsCard from '../components/ProductsCard'

// export default function Products() {

// 	const [products, setProducts] = useState([]);

// 	useEffect(() => {
// 		fetch('')
// 			.then(res => res.json())
// 			.then(data => {

// 				console.log(data)

// 				setProducts(data.map(product => {
// 					return (
// 						<ProductCard key={product._id} productprop={product} />
// 					);
// 				}));
// 			});
// 	}, []);

// 	return (
// 		<Fragment>
// 			<h1>Products</h1>
// 			{products}
// 		</Fragment>
// 	)
// }

import { useState, useEffect, useContext } from 'react';
import React from 'react'
import { Container } from 'react-bootstrap'

import AdminPanel from './../components/AdminPanel.js';
import UserPanel from './../components/UserPanel.js';

import UserContext from './../UserContext';

export default function products() {

	const [products, setProducts] = useState([]);

	const { user } = useContext(UserContext);


	const fetchData = () => {
		let token = localStorage.getItem('token')

		fetch('https://capstone-2-app.herokuapp.com/products/', {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
			.then(result => result.json())
			.then(result => {
				console.log(result)

				setProducts(result)
			})
	}

	useEffect(() => {
		fetchData()
	}, [])

	return (
		<Container className="p-4">
			{(user.isAdmin === true) ?
				<AdminPanel productData={products} fetchData={fetchData} />
				:
				<UserPanel productData={products} />
			}
		</Container>
	)
}
