import React, { useState, useEffect, Fragment } from 'react'

import { Container, Table, Button, Modal, Form } from 'react-bootstrap'

import Swal from 'sweetalert2';



export default function AdminPanel(props) {
    console.log(props)
    const { productData, fetchData } = props;
    console.log(productData)

    const [productId, setProductId] = useState('');
    const [products, setProduct] = useState([]);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    const [showEdit, setShowEdit] = useState(false);
    const [showAdd, setShowAdd] = useState(false);

    let token = localStorage.getItem('token');

    const openAdd = () => setShowAdd(true);
    const closeAdd = () => setShowAdd(false);



    const openEdit = (productId) => {
        fetch(`https://capstone-2-app.herokuapp.com/products/${productId}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
            .then(result => result.json())
            .then(result => {

                console.log(result)

                setProductId(result._id);
                setName(result.name);
                setDescription(result.description);
                setPrice(result.price)
            })

        setShowEdit(true);
    }

    const closeEdit = () => {

        setShowEdit(false);
        setName("")
        setDescription("")
        setPrice(0)
    }

    useEffect(() => {
        const productsArr = productData.map((product) => {
            return (
                <tr key={product._id}>
                    <td className="td-desc">{product.name}</td>
                    <td className="td-desc">{product.description}</td>
                    <td className="td-desc">{product.price}</td>
                    <td className="td-desc">
                        {
                            (product.isActive === true) ?
                                <span>In Stock</span>
                                :
                                <span>Out of Stock</span>
                        }
                    </td>
                    <td>
                        <Button className="btn mr-3 mb-2" variant="outline-info" size="md"
                            onClick={() => openEdit(product._id)}>
                            Edit
                        </Button>

                        {
                            (product.isActive === true) ?
                                <Button className="btn mr-3 mb-2" variant="outline-warning" size="md"
                                    onClick={() => archiveToggle(product._id, product.isActive)}>
                                    Disable
                                </Button>
                                :

                                <Button className="btn mr-3 mb-2" variant="outline-success" size="md"
                                    onClick={() => unarchiveToggle(product._id, product.isActive)}>
                                    Enable
                                </Button>

                        }

                    </td>
                </tr>
            )
        })

        setProduct(productsArr)
    }, [productData])

    /*edit product*/
    const editProduct = (e, productId) => {

        e.preventDefault()

        fetch(`https://capstone-2-app.herokuapp.com/products/${productId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
            .then(result => result.json())
            .then(result => {
                fetchData()

                if (typeof result !== "undefined") {

                    Swal.fire({
                        title: 'Product successfully edited',
                        icon: "success",
                        width: 600,
                        padding: '3em',
                        background: '#fff url(https://i.imgur.com/0M90bAW.png)',
                        backdrop: `rgba(0,0,123,0.4)`
                    })

                    closeEdit();
                } else {

                    fetchData()

                    Swal.fire({
                        title: 'Oops, something went wrong.',
                        icon: "error",
                        width: 600,
                        padding: '3em',
                        background: '#fff url(https://i.imgur.com/0M90bAW.png)',
                        backdrop: `rgba(255,192,203,0.4)`
                    })
                }
            })
    }

    /*update product*/
    const archiveToggle = (productId, isActive) => {

        fetch(`https://capstone-2-app.herokuapp.com/products/${productId}/archive`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
                isActive: isActive
            })
        })
            .then(result => result.json())
            .then(result => {

                fetchData();
                if (result === true) {

                    Swal.fire({
                        title: 'Product successfully archived/unarchived.',
                        icon: "success",
                        width: 600,
                        padding: '3em',
                        background: '#fff url(https://i.imgur.com/0M90bAW.png)',
                        backdrop: `rgba(0,0,123,0.4)`
                    })

                } else {
                    fetchData();

                    Swal.fire({
                        title: 'Oops, something went wrong.',
                        icon: "error",
                        width: 600,
                        padding: '3em',
                        background: '#fff url(https://i.imgur.com/0M90bAW.png)',
                        backdrop: `rgba(255,192,203,0.4)`
                    })
                }
            })
    }

    const unarchiveToggle = (productId, isActive) => {
        fetch(`https://capstone-2-app.herokuapp.com/products/${productId}/archive`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
                isActive: isActive
            })
        })
            .then(result => result.json())
            .then(result => {
                fetchData();
                if (result === true) {

                    Swal.fire({
                        title: 'Product successfully archived/unarchived.',
                        icon: "success",
                        width: 600,
                        padding: '3em',
                        background: '#fff url(https://i.imgur.com/0M90bAW.png)',
                        backdrop: `rgba(0,0,123,0.4)`
                    })

                } else {
                    fetchData();

                    Swal.fire({
                        title: 'Oops, something went wrong.',
                        icon: "error",
                        width: 600,
                        padding: '3em',
                        background: '#fff url(https://i.imgur.com/0M90bAW.png)',
                        backdrop: `rgba(255,192,203,0.4)`
                    })
                }
            })
    }

    // const deleteToggle = (productId) => {
    // 	fetch(`https://apithetinker-cap3.herokuapp.com/api/products/${productId}/delete`, {
    // 		method: "DELETE",
    // 		headers: {
    // 			"Authorization": `Bearer ${token}`
    // 		}
    // 	})
    // 	.then(result => result.json())
    // 	.then(result => {
    // 		console.log(result)

    // 		fetchData();
    // 		if(result === true){

    // 			Swal.fire({
    // 			  title: 'Product successfully deleted.',
    // 			  icon: "success",
    // 			  width: 600,
    // 			  padding: '3em',
    // 			  background: '#fff url(https://i.imgur.com/0M90bAW.png)',
    // 			  backdrop: `rgba(0,0,123,0.4)`
    // 			})

    // 		} else {
    // 			fetchData();

    // 			Swal.fire({
    // 			  title: 'Oops, something went wrong.',
    // 			  icon: "error",
    // 			  width: 600,
    // 			  padding: '3em',
    // 			  background: '#fff url(https://i.imgur.com/0M90bAW.png)',
    // 			  backdrop: `rgba(255,192,203,0.4)` 
    // 			})
    // 		}
    // 	})
    // }

    const addProduct = (e) => {
        e.preventDefault()
        fetch('https://capstone-2-app.herokuapp.com/products/add-product', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
            .then(result => result.json())
            .then(result => {

                if (result === true) {
                    fetchData()

                    Swal.fire({
                        title: 'Product successfully added.',
                        icon: "success",
                        width: 600,
                        padding: '3em',
                        background: '#fff url(https://i.imgur.com/0M90bAW.png)',
                        backdrop: `rgba(0,0,123,0.4)`
                    })

                    setName("")
                    setDescription("")
                    setPrice(0)

                    closeAdd();

                } else {
                    fetchData();

                    Swal.fire({
                        title: 'Oops, something went wrong.',
                        icon: "error",
                        width: 600,
                        padding: '3em',
                        background: '#fff url(https://i.imgur.com/0M90bAW.png)',
                        backdrop: `rgba(255,192,203,0.4)`
                    })
                }
            })
    }

    return (
        <Container>
            <div>
                <h2 className="title-admin text-center">Admin Dashboard</h2>
                <div className="d-flex justify-content-end m-2">
                    <Button className="btn-block m-3" variant="info" onClick={openAdd}>Add A New Product</Button>
                </div>
            </div>
            <Table className="table">
                <thead>
                    <tr>
                        <th className="heading-table">Name</th>
                        <th className="heading-table">Description</th>
                        <th className="heading-table">Price</th>
                        <th className="heading-table">Stocks</th>
                        <th className="heading-table">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {products}
                </tbody>
            </Table>

            {/*edit*/}
            <Modal show={showEdit} onHide={closeEdit} className="Form">
                <Form onSubmit={(e) => editProduct(e, productId)}>
                    <Modal.Header>
                        <Modal.Title className="title-modal">Edit Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group controlId="productName">
                            <Form.Label className="td-desc">Product Name</Form.Label>
                            <Form.Control
                                type="text"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                            />
                        </Form.Group>
                        <Form.Group controlId="productDescription">
                            <Form.Label className="td-desc">Product Description</Form.Label>
                            <Form.Control
                                type="text"
                                value={description}
                                onChange={(e) => setDescription(e.target.value)}
                            />
                        </Form.Group>
                        <Form.Group controlId="productPrice">
                            <Form.Label className="td-desc">Product Price</Form.Label>
                            <Form.Control
                                type="number"
                                value={price}
                                onChange={(e) => setPrice(e.target.value)}
                            />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="info" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>

            <Modal show={showAdd} onHide={closeAdd} className="Form">
                <Form onSubmit={(e) => addProduct(e)}>
                    <Modal.Header className="title-modal">Add Product</Modal.Header>
                    <Modal.Body>
                        <Form.Group productId="productName">
                            <Form.Label className="td-desc">Product Name</Form.Label>
                            <Form.Control
                                type="text"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                            />
                        </Form.Group>
                        <Form.Group productId="productDescription">
                            <Form.Label className="td-desc">Product Description</Form.Label>
                            <Form.Control
                                type="text"
                                value={description}
                                onChange={(e) => setDescription(e.target.value)}
                            />
                        </Form.Group>
                        <Form.Group productId="productPrice">
                            <Form.Label className="td-desc">Product Price</Form.Label>
                            <Form.Control
                                type="number"
                                value={price}
                                onChange={(e) => setPrice(e.target.value)}
                            />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeAdd}>Close</Button>
                        <Button variant="info" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </Container>
    )
}
