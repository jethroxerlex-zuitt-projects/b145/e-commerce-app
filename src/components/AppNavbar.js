import { useContext } from 'react'
import { Fragment } from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'
import React from 'react'


export default function AppNavbar() {

  const { user } = useContext(UserContext)
  console.log(user)

  return (
    <Navbar bg="transparent" expand="lg">
      <Navbar.Brand href="/">
        <img
          src="/BTS.png"
          width="50"
          height="50"
          className="d-inline-block align-top mx-3"
        />
      </Navbar.Brand>
      <Navbar.Toggle />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Link as={Link} to="/">Home</Nav.Link>
          <Nav.Link as={Link} to="/products">Products</Nav.Link>

          {(user.id !== null) ?

            <Nav.Link as={Link} to="/logout">Logout</Nav.Link>

            :

            <Fragment>

              <Nav.Link as={Link} to="/login">Login</Nav.Link>
              <Nav.Link as={Link} to="/register">Register</Nav.Link>
            </Fragment>
          }
          <Nav.Link as={Link} to="/cart">Cart</Nav.Link>

        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}

// import React, { Fragment, useContext } from 'react';
// import { Link, NavLink, useNavigate } from 'react-router-dom';

// import UserContext from './../UserContext';

// import { Navbar, Nav } from 'react-bootstrap';

// /*app navbar*/
// export default function AppNavbar() {
//   const { user, unsetUser } = useContext(UserContext);
//   let history = useNavigate();

//   const logout = () => {
//     unsetUser();
//     history.push('/login');
//   }

//   let leftNav = (user.id !== null) ?
//     (user.isAdmin === true) ?
//       <Fragment>
//         <Nav.Link className="link" as={NavLink} to="/add-product">Add product</Nav.Link>
//         <Nav.Link className="link" onClick={logout}>Logout</Nav.Link>
//       </Fragment>
//       :
//       <Fragment>
//         <Nav.Link className="link" as={NavLink} to="/details">Profile</Nav.Link>
//         <Nav.Link className="link" onClick={logout}>Logout</Nav.Link>
//       </Fragment>
//     :
//     (
//       <Fragment>
//         <Nav.Link className="link" as={NavLink} to="/register">Register</Nav.Link>
//         <Nav.Link className="link" as={NavLink} to="/login">Login</Nav.Link>
//       </Fragment>

//     )

//   return (
//     <Navbar className="nav" expand="lg" variant="dark">
//       <Navbar.Brand className="brand" as={Link} to="/">BTS One-Stop-Shop</Navbar.Brand>
//       <Navbar.Toggle aria-controls="basic-navbar-nav" />
//       <Navbar.Collapse id="basic-navbar-nav">
//         <Nav>
//           <Nav.Link className="link" as={NavLink} to="/">Home</Nav.Link>
//           <Nav.Link className="link" as={NavLink} to="/products">Products</Nav.Link>
//         </Nav>
//         <Nav>
//           {leftNav}
//         </Nav>
//       </Navbar.Collapse>
//     </Navbar>
//   )
// }
