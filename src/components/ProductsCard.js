// import { useState, useEffect } from 'react'
// import { Row, Col, Card, } from 'react-bootstrap'
// import { Link } from 'react-router-dom'

// export default function ProductsCard({ productprop }) {

// 	console.log(productprop)
// 	// console.log(typeof props)

// 	// deconstruct the courseProp into their own variables
// 	const { name, description, price, _id } = productprop

// 	return (
// 		<Row>
// 			<Col xs={12} md={4}>
// 				<Card className="cardHighlight p-3 mt-4" >
// 					<Card.Body>
// 						<Card.Title>
// 							<h2>{name}</h2>
// 						</Card.Title>
// 						<Card.Text>
// 							Description:
// 						</Card.Text>
// 						<Card.Subtitle>{description}</Card.Subtitle>
// 						<Card.Text>
// 							Price:
// 						</Card.Text>
// 						<Card.Subtitle>{price}</Card.Subtitle>
// 						<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
// 					</Card.Body>
// 				</Card>
// 			</Col>
// 		</Row>
// 	)
// }

import PropTypes from 'prop-types';
import React from 'react'
import { Card, Container } from 'react-bootstrap';

import { Link } from 'react-router-dom';


export default function Product({ productprop }) {
	console.log(productprop)

	const { name, description, price, _id } = productprop

	return (

		<Container className="card-container my-5 inline-block" xs={12} md={6}>
			<Card >
				<Card.Body className="cards">
					<Card.Title className="heading-highlights">{name}</Card.Title>
					<h5>Description</h5>
					<p>{description}</p>
					<h5>Price:</h5>
					<p>{price}</p>

					<Link className="btn btn-info" to={`/products/${_id}`}>
						Details
					</Link>
				</Card.Body>
			</Card>
		</Container>
	)
}


Product.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
