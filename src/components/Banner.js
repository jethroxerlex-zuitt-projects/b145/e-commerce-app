// import { Row, Col } from 'react-bootstrap'
// import { Link } from 'react-router-dom'
// import React from 'react'

// export default function Banner({ data }) {

// 	const { title, content, destination, label } = data

// 	return (
// 		<Row>
// 			<Col className="p-5">
// 				<h1>{title}</h1>
// 				<p>{content}</p>
// 				<Link to={destination}>{label}</Link>

// 			</Col>
// 		</Row>
// 	)
// }

import React from 'react';

import { Container, Row, Col, Button } from 'react-bootstrap'

import { NavLink } from 'react-router-dom'

export default function Banner() {
	return (

		<Container fluid>
			<Row>
				<Col className="px-0">
					<h1 className="heading text-center">BTS One Stop Shop</h1>
					<p className="desc text-center">Your happiness is our happiness</p>
					<div className="d-flex justify-content-center">
						<Button className="btn btn-info" size="mx" as={NavLink} to="/products">Order Now!</Button>
					</div>
				</Col>
			</Row>
		</Container>
	)
}