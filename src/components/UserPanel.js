import React, { useState, useEffect } from 'react'

import { Container } from 'react-bootstrap'

import Product from './ProductsCard'

export default function UserView({ productData }) {
    // console.log(productData)

    const [product, setProduct] = useState([])

    useEffect(() => {
        const productsArr = productData.map((product) => {
            if (product.isActive === true) {
                return <Product key={product._id} productprop={product} />
            } else {
                return null
            }

        })

        setProduct(productsArr)

    }, [productData])


    return (
        <Container>
            {product}
        </Container>
    )
}

